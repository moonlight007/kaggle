# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from textutils import review_to_sentences
from tqdm import tqdm
from gensim.models import word2vec
import pandas as pd
import nltk
import logging
import sys

reload(sys)
sys.setdefaultencoding("utf-8")
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)


logging.log(logging.INFO, "Reading the data")

train = pd.read_csv("../data/labeledTrainData.tsv", header=0, delimiter="\t", quoting=3)
unlabeled_train = pd.read_csv("../data/unlabeledTrainData.tsv", header=0, delimiter="\t", quoting=3)
test = pd.read_csv("../data/testData.tsv", header=0, delimiter="\t", quoting=3)
tokenizer = nltk.data.load(str('tokenizers/punkt/english.pickle'))


# Reading 100,000 samples
logging.log(logging.INFO, 'Read %d labeled training data, %d unlabeled training data and %d test data set' % \
      (train.shape[0], unlabeled_train.shape[0], test.shape[0])),

# Build the sentences for the word2vec model to be trained
sentences = []
for review in tqdm(train['review']):
    sentences += review_to_sentences(review.decode('utf-8'), tokenizer, False, False)

for review in tqdm(unlabeled_train['review']):
    sentences += review_to_sentences(review.decode('utf-8'), tokenizer, False, False)

# Train the word2vec model
# Set values for various parameters
num_features = 300    # Word vector dimensionality
min_word_count = 50   # Minimum word count
num_workers = 16       # Number of threads to run in parallel
context = 10        # Context window size
downsampling = 1e-3   # Downsample setting for frequent words

logging.log(logging.INFO, "Training word2vec model")
model = word2vec.Word2Vec(sentences, workers=num_workers, size=num_features, min_count=min_word_count,
                          window=context, sample=downsampling)
# Finish model training and start querying
model.init_sims(replace=True)
model_name = "../data/300features_40minwords_10context"
model.save(model_name)
print model.vocab

# Test out the model on some samples
print model.most_similar("man")
print model.most_similar("queen")
print model.most_similar("movie")
