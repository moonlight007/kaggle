from __future__ import unicode_literals
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cross_validation import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_auc_score
from tqdm import tqdm
from textutils import review_to_wordlist
import pandas as pd
import numpy as np
import nltk
import logging

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
train = pd.read_csv("../data/labeledTrainData.tsv", header=0, delimiter="\t", quoting=3)
unlabeled_train = pd.read_csv("../data/unlabeledTrainData.tsv", header=0, delimiter="\t", quoting=3)
test = pd.read_csv("../data/testData.tsv", header=0, delimiter="\t", quoting=3)
tokenizer = nltk.data.load(str('tokenizers/punkt/english.pickle'))


def get_feature_vectors(reviews):
    vectorizer = TfidfVectorizer(min_df=3,  max_features=None,
                                 strip_accents='unicode', analyzer='word', token_pattern=r'\w{1,}',
                                 ngram_range=(1, 4), use_idf=1, smooth_idf=1, sublinear_tf=1,
                                 stop_words = 'english')
    train_data_features = vectorizer.fit_transform(reviews)
    train_data_features = train_data_features.toarray()
    return train_data_features


def train_and_eval_auc(trained_model, train_x, train_y, test_x, test_y):
    trained_model.fit(train_x, train_y)
    p = trained_model.predict_proba(test_x)
    auc = roc_auc_score(test_y, p[:,1])
    return auc


def run_on_train_set():
    logging.info('starting the training and testing using cross validation')
    global train
    train_idx, test_idx = train_test_split(np.arange(len(train)), train_size=0.8, random_state=33)
    train_data = train.ix[train_idx]
    test_data = train.ix[test_idx]

    logging.log(logging.INFO, 'building training reviews')
    clean_train_reviews = []
    for review in tqdm(train_data['review']):
        clean_train_reviews.append(" ".join(review_to_wordlist(review, remove_stopwords=False)))
    train_data_vecs = get_feature_vectors(clean_train_reviews)

    clean_test_reviews = []
    for review in tqdm(test_data['review']):
        clean_test_reviews.append(" ".join(review_to_wordlist(review, remove_stopwords=False)))
    test_data_vecs = get_feature_vectors(clean_test_reviews)

    classifer = LogisticRegression()
    auc = train_and_eval_auc(classifer, train_data_vecs, train_data["sentiment"],
                             test_data_vecs, test_data["sentiment"].values)
    logging.info("Classifier AUC: %f" % auc)
    return classifer


def run_on_test_set(trained_model):
    logging.log(logging.INFO, 'Building test reviews')
    clean_test_reviews = []
    for review in tqdm(test['review']):
        clean_test_reviews.append(" ".join(review_to_wordlist(review, remove_stopwords=False)))
    test_data_vecs = get_feature_vectors(clean_test_reviews)

    result = trained_model.predict(test_data_vecs)

    logging.log(logging.INFO, 'Writing the predictions to the file')
    output = pd.DataFrame(data={'id': test['id'], 'sentiment': result})
    # Use pandas to write the comma-separated output file
    output.to_csv("../data/Word2Vec_Average_Vectors_model.csv", index=False, quoting=3)

if __name__ == '__main__':
    classifier = run_on_train_set()
    run_on_test_set(classifier)