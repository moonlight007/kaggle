from __future__ import unicode_literals
from gensim.models import Word2Vec
from sklearn.cross_validation import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import SGDClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import roc_auc_score
from tqdm import tqdm
from textutils import review_to_wordlist
import pandas as pd
import numpy as np
import nltk
import logging

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
train = pd.read_csv("../data/labeledTrainData.tsv", header=0, delimiter="\t", quoting=3)
unlabeled_train = pd.read_csv("../data/unlabeledTrainData.tsv", header=0, delimiter="\t", quoting=3)
test = pd.read_csv("../data/testData.tsv", header=0, delimiter="\t", quoting=3)
tokenizer = nltk.data.load(str('tokenizers/punkt/english.pickle'))
model = Word2Vec.load("../data/300features_40minwords_10context")


def make_feature_vector(words, model, num_features):
    feature_vector = np.zeros(num_features, dtype="float32")
    num_words = 1
    index2word = set(model.index2word)
    for word in words:
        if word in index2word:
            num_words += 1
            feature_vector = np.add(feature_vector, model[word])
    feature_vector = np.divide(feature_vector, num_words)
    return feature_vector


def get_avg_feature_vectors(reviews, model, num_features):
    counter = 0
    review_feature_vectors = np.zeros((len(reviews), num_features), dtype="float32")
    for review in reviews:
        review_feature_vectors[counter] = make_feature_vector(review, model, num_features)
        counter += 1.
    return review_feature_vectors


def train_and_eval_auc(trained_model, train_x, train_y, test_x, test_y):
    trained_model.fit(train_x, train_y)
    p = trained_model.predict_proba(test_x)
    auc = roc_auc_score(test_y, p[:,1])
    return auc


def run_on_train_set():
    logging.info('starting the training and testing using cross validation')
    global train
    train_idx, test_idx = train_test_split(np.arange(len(train)), train_size=0.8, random_state=44)
    train_data = train.ix[train_idx]
    test_data = train.ix[test_idx]

    logging.log(logging.INFO, 'building training reviews')
    clean_train_reviews = []
    for review in tqdm(train_data['review']):
        clean_train_reviews.append(review_to_wordlist(review, remove_stopwords=False))
    train_data_vecs = get_avg_feature_vectors(clean_train_reviews, model, model.syn0.shape[1])

    clean_test_reviews = []
    for review in tqdm(test_data['review']):
        clean_test_reviews.append(review_to_wordlist(review, remove_stopwords=False))
    test_data_vecs = get_avg_feature_vectors(clean_test_reviews, model, model.syn0.shape[1])

    classifer = LogisticRegression(penalty='l1', solver='liblinear', n_jobs=-1)
    # classifer = RandomForestClassifier(n_estimators=500, n_jobs=-1, verbose=1)
    # classifer = GradientBoostingClassifier(n_estimators=100, verbose=1)
    # classifer = SGDClassifier(n_jobs=-1, verbose=1)
    auc = train_and_eval_auc(classifer, train_data_vecs, train_data["sentiment"],
                             test_data_vecs, test_data["sentiment"].values)
    logging.info("Classifier AUC: %f" % auc)
    return classifer


def run_on_test_set(trained_model):
    logging.log(logging.INFO, 'Building test reviews')
    clean_test_reviews = []
    for review in tqdm(test['review']):
        clean_test_reviews.append(review_to_wordlist(review, remove_stopwords=False))
    test_data_vecs = get_avg_feature_vectors(clean_test_reviews, model, model.syn0.shape[1])

    result = trained_model.predict(test_data_vecs)

    logging.log(logging.INFO, 'Writing the predictions to the file')
    output = pd.DataFrame(data={'id': test['id'], 'sentiment': result})
    # Use pandas to write the comma-separated output file
    output.to_csv("../data/Bow-tfidf.csv", index=False, quoting=3)

if __name__ == '__main__':
    classifier = run_on_train_set()
    run_on_test_set(classifier)