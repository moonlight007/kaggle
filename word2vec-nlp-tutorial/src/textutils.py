# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
import re

stemmer = PorterStemmer()


def review_to_wordlist(review_text, remove_stopwords=False, use_stemmer=False):
    sanitized_text = BeautifulSoup(review_text, "lxml").get_text()
    letters_only = re.sub("[^a-zA-Z]", " ", sanitized_text)
    words = letters_only.lower().split(' ')

    # Make stopword removal optional as these prove benefitial for word2vec features
    if remove_stopwords:
        words = [w for w in words if w in set(stopwords.words("english"))]

    if use_stemmer:
        words = [stemmer.stem(w) for w in words]

    return words


def review_to_sentences(review_text, tokenizer, remove_stopwords=False, use_stemmer=False):
    raw_sentences = tokenizer.tokenize(review_text.strip())
    sentences = []
    for raw_sentence in raw_sentences:
        if len(raw_sentence) > 0:
            sentences.append(review_to_wordlist(raw_sentence, remove_stopwords, use_stemmer))
    return sentences
